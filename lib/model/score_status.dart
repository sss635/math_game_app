import 'package:flutter/widgets.dart';

class ScoreStatus with ChangeNotifier {
  int numDo = 0;
  int score = 0;

  void getNumDo() {
    numDo += 1;
    print(numDo);
    notifyListeners();
  }

  void getScore(int s) {
    score += s;
    print(score);
    notifyListeners();
  }

  void resetNumDo() {
    numDo = 0;
    print('rnum');
    notifyListeners();
  }

  void resetScore() {
    score = 0;
    print('rsc');
    notifyListeners();
  }
}
