import 'package:flutter/widgets.dart';

class ManageTimer with ChangeNotifier {
  bool isStop = false;

  void getStartTimerNow() {
    isStop = false;
    print(isStop);
    notifyListeners();
  }

  void getStopTimerNow() {
    isStop = true;
    print(isStop);
    notifyListeners();
  }
}
