import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:math';

import 'package:math_game_app/alert_dialog.dart';
import 'package:math_game_app/choice_button.dart';
import 'package:math_game_app/custom_icons.dart';
import 'package:math_game_app/menu_game.dart';
import 'package:provider/provider.dart';
import 'model/manage_timer.dart';
import 'model/score_status.dart';

import 'app_layout.dart';

//สร้างคะแนนและเวลาของแต่ละข้อ
class Status extends StatefulWidget {
  final double scale;
  const Status({super.key, required this.scale});

  @override
  _Status createState() => _Status(scale: this.scale);
}

class _Status extends State<Status> {
  int seconds = 5;
  Timer? timer;

  final double scale;

  _Status({required this.scale});

  void startTimer(BuildContext context, int score, ManageTimer time) {
    time.isStop;
    timer = Timer(const Duration(seconds: 1), () {
      if (mounted) {
        setState(() {
          if (!time.isStop) {
            if (seconds > 0) {
              seconds--;
            } else {
              stopTimer(context, score, time);
            }
          }
        });
      }
    });
  }

  void stopTimer(BuildContext context, int score, ManageTimer time) {
    timer?.cancel();
    time.isStop = true;
    createQuestion();
    showAlertDialog(
        context,
        const MenuGame(),
        const Multiply(),
        "Time Out",
        "\nCongratulation !! \nYour Score : $score \nPlay Again?");
  }

  @override
  Widget build(BuildContext context) {
    final isRunning = timer == null ? false : timer!.isActive;
    final time = Provider.of<ManageTimer>(context);
    print(time.isStop);
    final score = Provider.of<ScoreStatus>(context);
    if (!isRunning) {
      startTimer(context, score.score, time);
    }
    return LayoutBuilder(builder: (context, constraints) {
      final screenHeight = MediaQuery.of(context).size.height;
      final landscapeHeight =
          screenHeight < AppLayout.widthFirstBreakpoint + 50;
      return Container(
        margin: EdgeInsets.only(left: 20.0, right: 20.0, bottom: landscapeHeight? 0:20),
        child: Column(
          children: [
            const Divider(
              color: Colors.brown,
              thickness: 2,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: showStatus(scale, seconds, landscapeHeight, context),
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(top: landscapeHeight ? 0 : 25),
                    height: landscapeHeight ? 30 : 50,
                    child: Text(
                      "Time: $seconds",
                      style: GoogleFonts.ubuntuCondensed(
                        fontWeight: FontWeight.w600,
                        color: Colors.brown[700],
                        fontSize: landscapeHeight ? 20 : 25,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
              ],
            ),
            const Divider(
              color: Colors.brown,
              thickness: 2,
            ),
          ],
        ),
      );
    });
  }
}

//โชว์สถานะของเวลาและคะแนน
Widget showStatus(double scale, int seconds, bool landscapeHeight, BuildContext context) {
  final updateScore = Provider.of<ScoreStatus>(context);
  return SizedBox(
      child: Text.rich(
    TextSpan(
        style: GoogleFonts.ubuntuCondensed(
          fontWeight: FontWeight.w600,
          color: Colors.brown[700],
          fontSize: landscapeHeight ? 20 : 25,
        ),
        children: [
          const TextSpan(
              text: ' SCORE : ',
              style: TextStyle(
                  fontStyle: FontStyle.italic, fontWeight: FontWeight.bold)),
          TextSpan(
              text: ' ${updateScore.score}',
              style: TextStyle(
                  fontSize: landscapeHeight ? 25 : 30,
                  color: (updateScore.score > 0)
                      ? const Color.fromARGB(255, 26, 94, 29)
                      : const Color.fromARGB(255, 146, 42, 34))),
        ]),
    textAlign: TextAlign.center,
  ));
}

class Multiply extends StatelessWidget {

  const Multiply({super.key});


  @override
  Widget build(BuildContext context) {
    final time = Provider.of<ManageTimer>(context);
    final score = Provider.of<ScoreStatus>(context);
    if(!time.isStop){
      time.isStop = false;
      createQuestion();
    }

    return LayoutBuilder(
      builder: (context, constraints) {
        final double scalePortrait = constraints.maxWidth / 390;
        final screenHeight = MediaQuery.of(context).size.height;
        final landscapeHeight =
            screenHeight < AppLayout.widthFirstBreakpoint + 50;
        return Scaffold(
            backgroundColor: Colors.pink[100],
            appBar: AppBar(
              leading: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                  score.resetScore();
                },
                icon: Icon(
                  CustomIconsArrow.left_open_outline,
                  size: landscapeHeight ? 27 : 35,
                  color: Colors.brown,
                ),
              ),
              title: Text(
                "Multiply Game",
                style: GoogleFonts.ubuntuCondensed(
                    fontWeight: FontWeight.w600,
                    color: Colors.brown[700],
                    fontSize: 14),
                textScaleFactor: landscapeHeight ? 1.8 : 2.3,
                textAlign: TextAlign.center,
              ),
              elevation: 0,
              backgroundColor: Colors.pink[100],
            ),
            body: multiplyPage(scalePortrait, context, landscapeHeight));
      },
    );
  }
}

//สร้างโจทย์และช้อยส์
List<int> randomQuestion() {
  var rng = Random();
  List<int> listQuestion = [];
  int num1 = rng.nextInt(11) + 2;
  int num2 = rng.nextInt(11) + 2;
  int answer = num1 * num2;
  listQuestion.add(num1);
  listQuestion.add(num2);
  listQuestion.add(answer);
  return listQuestion;
}

List<int> randomChoice(int answer) {
  var rng = Random();
  List<int> listChoice = [];
  while (listChoice.length < 4) {
    int choice = answer;
    while (choice == answer) {
      if (answer > 10) {
        choice = rng.nextInt(answer - (answer - 10)) + (answer - 10);
      } else {
        choice = rng.nextInt(answer + 5) + 1;
      }
    }
    if (listChoice.isEmpty) {
      listChoice.add(choice);
    } else {
      bool isNumInList = false;
      for (int i = 0; i < listChoice.length; i++) {
        if (choice == listChoice[i]) {
          isNumInList = true;
          break;
        }
      }
      if (!isNumInList) {
        listChoice.add(choice);
      }
    }
    if (listChoice.length == 4) {
      int answerIndex = rng.nextInt(4);
      listChoice[answerIndex] = answer;
    }
  }
  return listChoice;
}

//สำหรับข้อแรก
List<int>? listQuestion = randomQuestion();
List<int>? listChoice = randomChoice(listQuestion![2]);
String? question = "${listQuestion![0]} \u00d7 ${listQuestion![1]}";

//สำหรับข้อ2ขึ้นไป
void createQuestion() {
  listQuestion = randomQuestion();
  listChoice = randomChoice(listQuestion![2]);
  question = "${listQuestion![0]} \u00d7 ${listQuestion![1]}";
}

//จัดการหน้าจอ
bool? isPortrait;

Widget multiplyPage(
    double scale, BuildContext context, bool landscapeHeight) {
  isPortrait = true;
  double sizeChl = landscapeHeight ? 2.1 : 2.5;
  final screenWidth = MediaQuery.of(context).size.width;
  final screenHeight = MediaQuery.of(context).size.height;
  final smallerDevice = screenWidth < AppLayout.widthFirstBreakpoint;
  final heightDevice = screenHeight > AppLayout.heightFirstBreakpoint;
  return SizedBox(
      child: Column(
        children: [
          Status(scale: scale),
          Row(
            children: [
              Expanded(flex: landscapeHeight ? 2 : 1, child: const Text("")),
              Expanded(
                  flex: 1,
                  child: styleQuesWidget("${listQuestion![0]}", sizeChl)),
              const Expanded(
                  flex: 1,
                  child: Icon(CustomIcons.cancel_outline,
                      size: 25, color: Colors.brown)),
              Expanded(
                  flex: 1,
                  child: styleQuesWidget("${listQuestion![1]}", sizeChl)),
              Expanded(flex: 1, child: styleQuesWidget("=", sizeChl + 0.7)),
              const Expanded(
                flex: 1,
                child: Icon(
                    CustomIconsArrow
                        .help_circled_alt, //.question_1, //.question,
                    size: 40,
                    color: Colors.brown),
              ),
              Expanded(flex: landscapeHeight ? 2 : 1, child: const Text(""))
            ],
          ),
          SizedBox(
            height: landscapeHeight ? 10 : 30,
          ),
          Flexible(
              child: listChoiceButton(
                  smallerDevice, heightDevice, scale, context)),
        ],
      ));
}

Widget styleQuesWidget(String textQ, double sizeTextChl) {
  return Text(
    textQ,
    style: GoogleFonts.ubuntuCondensed(
      fontWeight: FontWeight.w700,
      color: Colors.brown[700],
    ),
    textScaleFactor: sizeTextChl,
    textAlign: TextAlign.center,
  );
}

GridView listChoiceButton(bool smallerDevice, bool heightDevice, double scale, BuildContext context) {
  final choiceList = [
    choiceButton(listQuestion![2], listChoice![0], context),
    choiceButton(listQuestion![2], listChoice![1], context),
    choiceButton(listQuestion![2], listChoice![2], context),
    choiceButton(listQuestion![2], listChoice![3], context),
  ];

  final choiceGridS = GridView.count(
      crossAxisCount: smallerDevice ? 2 : 4,
      childAspectRatio: smallerDevice ? 1.2 : 1.3,
      children: choiceList);
  final choiceGridM = GridView.count(
      crossAxisCount: heightDevice ? 2 : 4,
      childAspectRatio: heightDevice ? 1.1 : 1.2,
      children: choiceList);

  final choiceGrid = !smallerDevice && heightDevice ? choiceGridM : choiceGridS;
  return choiceGrid;
}

Widget choiceButton(
    int answer, int num, BuildContext context) {
  bool checkAnswer(int answer, int num) {
    if (num == answer) {
      return true;
    }
    return false;
  }
  final time = Provider.of<ManageTimer>(context);
  final score = Provider.of<ScoreStatus>(context);

  return ChoiceButton(
      num: "$num",
      onPressed: () {
        if (checkAnswer(answer, num)) {
          score.getScore(10);
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => const Multiply()),
          );
        } else {
          time.getStopTimerNow();
          showAlertDialog(
              context,
              const MenuGame(),
              const Multiply(),
              "Incorrect",
              "\nCongratulation !! \nYour Score : ${score.score} \nPlay Again?");
        }
      },
      colorBg: Colors.pinkAccent);
}
