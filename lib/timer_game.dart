import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:async';
import 'alert_dialog.dart';
import '../menu_game.dart';
import 'package:provider/provider.dart';
import 'model/manage_timer.dart';
import 'model/score_status.dart';

bool isStop = false;
int num = 0;
int score = 0;

class TimerGame extends StatefulWidget {
  final int wSeconds;
  // final int wMinutes;
  // final int wHours;
  // const TimerGame({
  //   Key? key,
  //   required this.wSeconds,
  //   required this.wMinutes,
  //   required this.wHours,
  // }) : super(key: key);
  final Widget thisGame;
  // const TimerGame({super.key});
  const TimerGame({
    Key? key,
    required this.wSeconds,
    required this.thisGame,
  }) : super(key: key);
  @override
  State<TimerGame> createState() =>
      _TimerGameState(nowGame: this.thisGame, wSeconds: this.wSeconds);
}

class _TimerGameState extends State<TimerGame> {
  final Widget nowGame;
  final int wSeconds;

  _TimerGameState({
    Key? key,
    required this.nowGame,
    required this.wSeconds,
  });

  int _seconds = 10;
  int _minutes = 0;
  int _hours = 0;

  @override
  void initState() {
    super.initState();
    _seconds = wSeconds;
    _startTimer();
  }

  bool _isRunning = false;

  Timer? _timer;

  void _startTimer() {
    setState(() {
      _isRunning = true;
    });
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        if (isStop == true) {
          _isRunning = false;
          _cancelTimer();
        } else if (_seconds > 0) {
          _seconds--;
        } else {
          if (_minutes > 0) {
            _minutes--;
            _seconds = 59;
          } else {
            if (_hours > 0) {
              _hours--;
              _minutes = 59;
              _seconds = 59;
            } else {
              _isRunning = false;
              _timer?.cancel();
              showAlertDialog(
                  context,
                  const MenuGame(),
                  nowGame,
                  "Time Out !!",
                  // "PlayAgain ?");
                  "\nCongratulation !! \nYou Do : $num \nYour Score : $score \nPlay Again?");
            }
          }
        }
      });
    });
  }

  void _pauseTimer() {
    setState(() {
      _isRunning = false;
    });
    _timer?.cancel();
  }

  void _cancelTimer() {
    setState(() {
      _hours = 0;
      _minutes = 0;
      _seconds = 0;
      _isRunning = false;
    });
    _timer?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    final isStopm = Provider.of<ManageTimer>(context);
    isStop = isStopm.isStop;
    final stat = Provider.of<ScoreStatus>(context);
    num = stat.numDo;
    score = stat.score;
    return Center(
        child: Text(
      '${_hours.toString().padLeft(2, '0')}:${_minutes.toString().padLeft(2, '0')}:${_seconds.toString().padLeft(2, '0')}',
      //   style: const TextStyle(
      //       fontSize: 20, fontWeight: FontWeight.bold, color: Colors.brown),
      // ),
      style: GoogleFonts.ubuntuCondensed(
          fontWeight: FontWeight.w500, color: Colors.brown[700], fontSize: 25),
    )
        // child: Column(
        //   mainAxisAlignment: MainAxisAlignment.start,
        //   children: [
        // Container(
        //   width: double.infinity,
        //   height: 85,
        //   color: Colors.amber,
        //   child: Center(
        //     child: Text(
        //       '${_hours.toString().padLeft(2, '0')}:${_minutes.toString().padLeft(2, '0')}:${_seconds.toString().padLeft(2, '0')}',
        //       style:
        //           const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        //     ),
        //   ),
        // ),
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //   children: [
        //     Text(
        //       '${_hours.toString().padLeft(2, '0')}:${_minutes.toString().padLeft(2, '0')}:${_seconds.toString().padLeft(2, '0')}',
        //       style:
        //           const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        //     ),
        //     IconButton(
        //         onPressed: () {
        //           if (_isRunning) {
        //             _pauseTimer();
        //           } else {
        //             _startTimer();
        //           }
        //         },
        //         icon: _isRunning
        //             ? const Icon(Icons.pause)
        //             : const Icon(Icons.play_circle_fill)),
        //     IconButton(
        //         onPressed: _cancelTimer, icon: const Icon(Icons.cancel)),

        //     // ElevatedButton(
        //     //   onPressed: () {
        //     //     if (_isRunning) {
        //     //       _pauseTimer();
        //     //     } else {
        //     //       _startTimer();
        //     //     }
        //     //   },
        //     //   style: ElevatedButton.styleFrom(fixedSize: const Size(150, 40)),
        //     //   child: _isRunning ? const Text('Pause') : const Text('Start'),
        //     // ),
        //     // ElevatedButton(
        //     //   onPressed: _cancelTimer,
        //     //   style: ElevatedButton.styleFrom(
        //     //       backgroundColor: Colors.red,
        //     //       fixedSize: const Size(150, 40)),
        //     //   child: const Text('Cancel'),
        //     // ),
        //   ],
        // ),
        // ],
        // ),
        );
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
    }
    super.dispose();
  }
}
