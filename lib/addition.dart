import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:math_game_app/app_layout.dart';
import 'dart:math';

import 'package:math_game_app/custom_icons.dart';
import 'menu_game.dart';
import 'challenge_question.dart';
import 'choice_button.dart';
import 'alert_dialog.dart';
import 'dart:async';
import 'package:provider/provider.dart';
import 'model/manage_timer.dart';
import 'title_game.dart';
import 'timer_game.dart';
import 'model/score_status.dart';

double sizeTextChl = 2.5;
Color? colorChoiceBtn = Colors.brown[700];
int num1 = 0;
int num2 = 0;
int result = 0;

// int score = 0;
// int numQ = 0;
String title = "You're lose !!";
String message = "Play Again?";
// bool isStop = false;

// class Addition extends StatefulWidget {
class Addition extends StatelessWidget {
// class Substract extends StatefulWidget {
  const Addition({super.key});

  // final int thisScore;
  // final int numDo;

  // const Addition({
  //   Key? key,
  //   required this.thisScore,
  //   required this.numDo,
  // }) : super(key: key);


  // @override
  // State<Addition> createState() =>
  //     _AdditionState(nowScore: this.thisScore, nowDo: this.numDo);


// class Addition extends StatelessWidget {
//   const Addition({super.key});
//
//   @override
//   State<Addition> createState() => _Addition();
//   // Widget build(BuildContext context) {
//   //   return Scaffold(
//   //       backgroundColor: Colors.white,
//   //       body: Column(
//   //         children:<Widget> [
//   //           titleWidget(),
//   //           Cal(),
//   //         ],
//   //       )
//   //   );
//   // }
// }
// class Cal extends StatefulWidget {
//   const Cal({super.key});
//   @override
//   State<Cal> createState() => _Cal();
// }
// class _AdditionState extends State<Addition> {
//   final int nowScore;
//   final int nowDo;

  // _AdditionState({
  //   Key? key,
  //   required this.nowScore,
  //   required this.nowDo,
  // });

  // @override
  // void initState() {
  //   super.initState();
  //   // ori = true;
  //   // play(0);
  //   play(1);
  //   score = nowScore;
  //   numQ = nowDo;
  //   // isStop = false;
  // }

  init() {
    num1 = Random().nextInt(90) + 10;
    num2 = Random().nextInt(90) + 10;
    result = num1 + num2;
  }

  ///////////
  play() {
    // print("i $reset $ori");
    // if (reset == 0) {
    //   score = 0;
    //   numQ = 0;
    //   // print("y $reset");
    // }
    num1 = Random().nextInt(90) + 10;
    num2 = Random().nextInt(90) + 10;
    result = num1 + num2;
  }

  // trOrFl(int num, int randomCr) {
  //   if (num == randomCr) {
  //     setState(() {
  //       score += 10;
  //       numQ++;
  //       // ori = false;
  //       // play(1);
  //     });
  //     Navigator.pushReplacement(
  //       context,
  //       MaterialPageRoute(
  //           builder: (context) => Addition(thisScore: score, numDo: numQ)),
  //     );
  //   } else {
  //     setState(() {
  //       score -= 12;
  //       numQ++;
  //       play(1);
  //       // ori = false;
  //     });
  //     if (score > 0) {
  //       Navigator.pushReplacement(
  //         context,
  //         MaterialPageRoute(
  //             builder: (context) => Addition(thisScore: score, numDo: numQ)),
  //       );
  //     }
  //   }
  // }

  // genChoice(bool smallerDevice, bool heightDevice) {
genChoice(bool smallerDevice, bool heightDevice, BuildContext context) {
    final isStopm = Provider.of<ManageTimer>(context);
    final stat = Provider.of<ScoreStatus>(context);
    var ch = [0, 0, 0, 0];
    int randomCr = Random().nextInt(4);
    ch[randomCr] = result;
    var rd = 0;
    for (int i = 0; i < ch.length; i++) {
      if (i != randomCr) {
        rd = Random().nextInt(70) + 10;
        if (!ch.contains(rd)) {
          ch[i] = rd;
        }
      }
    }
    print(ch);
    print(randomCr);

    final choiceList = [
      ChoiceButton(
        num: '${ch[0]}',
        onPressed: () {
           if (0 == randomCr) {
            stat.getScore(10);
            stat.getNumDo();
            }else{
              stat.getScore(-12);
              stat.getNumDo();
            }
            if (stat.score > 0) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => const Addition()),
            );
          } else if (stat.score <= 0) {
            showAlertDialog(context, const MenuGame(), const Addition(), title,
                "\nCongratulation !! \nYou Do : ${stat.numDo} \nYour Score : ${stat.score} \nPlay Again?");
            isStopm.getStopTimerNow();
          }  
        },
        colorBg: Colors.orange[800], //colorChoiceBtn
      ),
      ChoiceButton(
        num: '${ch[1]}',
        onPressed: () {
          if (1 == randomCr) {
            stat.getScore(10);
            stat.getNumDo();
          }else{
            stat.getScore(-12);
            stat.getNumDo();
          }
          if (stat.score > 0) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => const Addition()),
            );
          } else if (stat.score <= 0) {
            showAlertDialog(context, const MenuGame(), const Addition(), title,
                "\nCongratulation !! \nYou Do : ${stat.numDo} \nYour Score : ${stat.score} \nPlay Again?");
            isStopm.getStopTimerNow();
          } 
        },
        colorBg: Colors.orange[600], // Colors.brown[800]
      ),
      ChoiceButton(
        num: '${ch[2]}',
        onPressed: () {
           if (2 == randomCr) {
            stat.getScore(10);
            stat.getNumDo();
          }else{
            stat.getScore(-12);
            stat.getNumDo();
          }
          if (stat.score > 0) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => const Addition()),
            );
          } else if (stat.score <= 0) {
            showAlertDialog(context, const MenuGame(), const Addition(), title,
                "\nCongratulation !! \nYou Do : ${stat.numDo} \nYour Score : ${stat.score} \nPlay Again?");
            isStopm.getStopTimerNow();
          }  
        },
        colorBg: Colors.orange[600], // Colors.brown[800]
      ),
      ChoiceButton(
        num: '${ch[3]}',
        onPressed: () {
          if (3 == randomCr) {
            stat.getScore(10);
            stat.getNumDo();
          } else {
            stat.getScore(-12);
            stat.getNumDo();
          }
          if (stat.score > 0) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => const Addition()),
            );
          } else if (stat.score <= 0) {
            showAlertDialog(context, const MenuGame(), const Addition(), title,
                "\nCongratulation !! \nYou Do : ${stat.numDo} \nYour Score : ${stat.score} \nPlay Again?");
            isStopm.getStopTimerNow();
          }

        },
        colorBg: Colors.orange[800], // Colors.brown[800]
      ),
    ];

    final choiceGridS = GridView.count(
        crossAxisCount: smallerDevice ? 2 : 4,
        childAspectRatio: smallerDevice ? 1.2 : 1.3,
        children: choiceList);
    final choiceGridM = GridView.count(
        crossAxisCount: heightDevice ? 2 : 4,
        childAspectRatio: heightDevice ? 1.1 : 1.2,
        children: choiceList);

    final choiceGrid =
        !smallerDevice && heightDevice ? choiceGridM : choiceGridS;
    return choiceGrid;
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    final smallerDevice = screenWidth < AppLayout.widthFirstBreakpoint;
    final heightDevice = screenHeight > AppLayout.heightFirstBreakpoint;
    final landscapeHeight = screenHeight < AppLayout.widthFirstBreakpoint + 50;

    return Scaffold(
        backgroundColor: Colors.orange[100],
        // backgroundColor: Colors.brown[50],
        body: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Container(
                alignment: Alignment.center,
                constraints: const BoxConstraints(
                  maxWidth: 1700,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(child: play()),
                    TitlePage(
                      title: "Addition Game",
                      iconBack: Icon(
                        CustomIconsArrow
                            .left_open_outline, //left_outline, //arrow_alt_circle_left,
                        size: landscapeHeight ? 27 : 35,
                        color: Colors.brown,
                      ),
                      landscapeHeight: landscapeHeight,
                    ),
                    const Divider(
                      color: Colors.brown,
                      thickness: 2,
                    ),
                     statusWidget(landscapeHeight, smallerDevice, context),
                    const Divider(
                      color: Colors.brown,
                      thickness: 2,
                    ),
                    ChallengeQuestion(
                      num1: num1,
                      iconOpr: const Icon(CustomIcons.plus_outline,
                          size: 25, color: Colors.brown), //Colors.brown
                      num2: num2,
                      sizeTextChl: sizeTextChl,
                      iconAns: const Icon(
                          CustomIconsArrow
                              .help_circled_alt, //.question_1, //.question,
                          size: 40,
                          color: Colors.brown),
                      landscapeHeight: landscapeHeight,
                    ),
                    Flexible(
                      // child: genChoice(smallerDevice, heightDevice),
                      child: genChoice(smallerDevice, heightDevice, context),
                    )
                  ],
                )

                // Padding(padding: EdgeInsets.all(5.0)),
                // Container( child: init(),  ),
                // Text('$num1 + $num2'),
                // Text('$result'),
                // IconButton(
                //   icon: const Icon(Icons.play_arrow),
                //   tooltip: 'Next Exercise',
                //   onPressed: () {
                //     setState(() {
                //       init() ;
                //     });
                //   },
                // ),
                // Center(
                //   child: ElevatedButton(
                //     onPressed: () {
                //       Navigator.pop(context);
                //     },
                //     child: const Text('Go back!'),
                //   ),
                // ),
                )));
  }


// Widget titleWidget() {
//   return const Padding(
//       padding: EdgeInsets.only(top: 26.0, bottom: 10.0),
//       child: Text(
//         "Addition Game",
//         style: TextStyle(
//           fontWeight: FontWeight.bold,
//           color: Colors.brown,
//         ),
//         textScaleFactor: 2.1,
//         textAlign: TextAlign.center,
//       ));
// }
}
Widget statusWidget(
    bool landscapeHeight, bool smallerDevice, BuildContext context) {
  return smallerDevice
      ? Column(
          children: [
            showScoreWidget(landscapeHeight, context),
            Container(
              height: landscapeHeight ? 30 : 50,
              // child: const TimerGame(),
              child: TimerGame(
                  thisGame: Addition(),
                  wSeconds: 7,
                ),

            ),
          ],
        )
      : Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 4,
              child: showScoreWidget(landscapeHeight, context),
            ),
            Expanded(
              flex: 3,
              child: Container(
                height: landscapeHeight ? 30 : 50,
                // child: const TimerGame(),
                child: TimerGame(
                thisGame: Addition(),
                wSeconds: 7,
              ),

              ),
            )
          ],
        );
}

Widget showScoreWidget(bool landscapeHeight, BuildContext context) {
  final stat = Provider.of<ScoreStatus>(context);
  return Container(
      // color: Color.fromARGB(255, 227, 187, 141),
      height: landscapeHeight ? 30 : 50,
      child: Center(
        child: Text.rich(
          TextSpan(
            // text: 'DO : $numQ', // default text style
            // style: TextStyle(
            //     fontSize: landscapeHeight ? 15 : 20,
            //     color: Colors.brown[700],
            //     fontWeight: FontWeight.bold),
            style: GoogleFonts.ubuntuCondensed(
                fontWeight: FontWeight.w600,
                color: Colors.brown[700],
                fontSize: landscapeHeight ? 20 : 25),
            children: <TextSpan>[
              const TextSpan(
                  text: 'DO : ', style: TextStyle(fontWeight: FontWeight.bold)),
              TextSpan(
                  text: ' ${stat.numDo}  | ',
                  style: TextStyle(
                    fontSize: landscapeHeight ? 25 : 30,
                  )),
              const TextSpan(
                  text: ' SCORE : ',
                  style: TextStyle(
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold)),
              TextSpan(
                  text: ' ${stat.score}',
                  style: TextStyle(
                      fontSize: landscapeHeight ? 25 : 30,
                      color: (score > 5)
                          ? const Color.fromARGB(255, 26, 94, 29)
                          : const Color.fromARGB(255, 146, 42, 34))),
            ],
          ),
        ),
      ));
}

// class TimerGame extends StatefulWidget {
//   const TimerGame({super.key});
//   @override
//   State<TimerGame> createState() => _TimerGameState();
// }

// class _TimerGameState extends State<TimerGame> {
//   @override
//   void initState() {
//     super.initState();

//     _startTimer();
//   }

//   int _seconds = 5;
//   int _minutes = 0;
//   int _hours = 0;

//   bool _isRunning = false;

//   Timer? _timer;

//   void _startTimer() {
//     setState(() {
//       _isRunning = true;
//     });
//     _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
//       setState(() {
//         if (isStop == true) {
//           _isRunning = false;
//           _cancelTimer();
//         } else if (_seconds > 0) {
//           _seconds--;
//         } else {
//           if (_minutes > 0) {
//             _minutes--;
//             _seconds = 59;
//           } else {
//             if (_hours > 0) {
//               _hours--;
//               _minutes = 59;
//               _seconds = 59;
//             } else {
//               _isRunning = false;
//               _timer?.cancel();
//               showAlertDialog(
//                   context,
//                   const MenuGame(),
//                   const Addition(thisScore: 0, numDo: 0),
//                   "Time Out !!",
//                   "\nCongratulation !! \nYou Do : $numQ \nYour Score : $score \nPlay Again?");
//             }
//           }
//         }
//       });
//     });
//   }

//   void _pauseTimer() {
//     setState(() {
//       _isRunning = false;
//     });
//     _timer?.cancel();
//   }

//   void _cancelTimer() {
//     setState(() {
//       _hours = 0;
//       _minutes = 0;
//       _seconds = 0;
//       _isRunning = false;
//     });
//     _timer?.cancel();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Center(
//       child: Text(
//         '${_hours.toString().padLeft(2, '0')}:${_minutes.toString().padLeft(2, '0')}:${_seconds.toString().padLeft(2, '0')}',
//         // style: const TextStyle(
//         //     fontSize: 20, fontWeight: FontWeight.bold, color: Colors.brown),
//         style: GoogleFonts.ubuntuCondensed(
//             fontWeight: FontWeight.w500,
//             color: Colors.brown[700],
//             fontSize: 25),
//       ),
//     );
//   }

//   @override
//   void dispose() {
//     if (_timer != null) {
//       _timer!.cancel();
//     }
//     super.dispose();
//   }
// }

// class TitlePage extends StatelessWidget {
//   final String title;
//   final Icon iconBack;
//   final bool landscapeHeight;
//   // final BuildContext context;
//   const TitlePage({
//     Key? key,
//     required this.title,
//     required this.iconBack,
//     required this.landscapeHeight,
//     // required this.context,
//   }) : super(key: key);

//   // Widget titleWidget(context) {
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: landscapeHeight
//           ? const EdgeInsets.only(top: 10.0, bottom: 5.0)
//           : const EdgeInsets.only(top: 26.0, bottom: 10.0),
//       child: Row(
//         children: [
//           Expanded(
//             flex: 1,
//             child: IconButton(
//                 onPressed: () {
//                   isStop = true;
//                   Navigator.pop(context);
//                   // showAlertDialog(context, null, const MenuGame(),
//                   //     "Back to Menu Game !!", "Are you sure !?");
//                   // Navigator.push(
//                   //   context,
//                   //   MaterialPageRoute(builder: (context) => const MenuGame()),
//                   // );
//                 },
//                 icon: iconBack),
//           ),
//           Expanded(
//             flex: 5,
//             child: Text(
//               title,
//               // style: const TextStyle(
//               //   fontWeight: FontWeight.bold,
//               //   color: Colors.brown,
//               // ),
//               style: GoogleFonts.ubuntuCondensed(
//                 fontWeight: FontWeight.w600,
//                 color: Colors.brown[700],
//               ),
//               textScaleFactor: landscapeHeight ? 1.8 : 2.3,
//               textAlign: TextAlign.center,
//             ),
//           ),
//           const Expanded(flex: 1, child: Text(""))
//         ],
//       ),
//     );
//   }
// }
