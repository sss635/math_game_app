import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ChoiceButton extends StatelessWidget {
  final String num;
  final Function() onPressed;
  final Color? colorBg;
  const ChoiceButton({
    Key? key,
    required this.num,
    required this.onPressed,
    required this.colorBg,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: ElevatedButton(
        onPressed: onPressed,
        style: ButtonStyle(backgroundColor: MaterialStateProperty.all(colorBg)),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Text(
            num,
            // style: const TextStyle(
            //   fontWeight: FontWeight.bold,
            // ),
            style: GoogleFonts.ubuntuCondensed(
              fontWeight: FontWeight.w700,
            ),
            textScaleFactor: 2.5,
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
