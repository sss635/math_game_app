import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:math_game_app/home.dart';
import 'package:provider/provider.dart';
// import 'menu_game.dart';
import 'model/manage_timer.dart';
import 'model/score_status.dart';

void main() {
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(
      create: (context) => ManageTimer(),
    ),
    ChangeNotifierProvider(
      create: (context) => ScoreStatus(),
    ),
  ], child: const MathGameApp()));
}

class MathGameApp extends StatelessWidget {
  const MathGameApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'A smartphone app',
        theme: ThemeData(
          primarySwatch: Colors.pink,
        ),
        home: LayoutBuilder(
          builder: (context, constraints) {
            final double scalePortrait = constraints.maxWidth / 390;
            final double scaleLandscape = constraints.maxHeight / 390;
            return constraints.maxWidth < constraints.maxHeight
                ? HomePage(scale: scalePortrait)
                : HomePage(scale: scaleLandscape);
          },
        ),
        // home: const Scaffold(
        //   backgroundColor: Colors.white,
        //   // appBar: AppBar(
        //   //   title: const Text('Math Game App'),
        //   // ),
        //   body: MenuGame(), // Container(),
        // ),
      ),
    );
  }
}
