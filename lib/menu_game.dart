import 'dart:html';

import 'package:flutter/material.dart';
import 'package:math_game_app/multiply.dart';
import 'custom_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'model/manage_timer.dart';
import 'model/score_status.dart';

import 'substract.dart';
import 'app_layout.dart';
import 'addition.dart';
import 'division.dart';

class MenuGame extends StatelessWidget {
  const MenuGame({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    final smallerDevice = screenWidth < AppLayout.widthFirstBreakpoint;
    final heightDevice = screenHeight > AppLayout.heightFirstBreakpoint;
    final landscapeHeight = screenHeight < AppLayout.widthFirstBreakpoint + 50;
    final isStop = Provider.of<ManageTimer>(context);
    final stat = Provider.of<ScoreStatus>(context);

    final menuList = [
      _MenuButton(
        icon: CustomIcons.plus_outline,
        title: 'Addition',
        color: Colors.orange,
        onPressed: () {
          isStop.getStartTimerNow();
          stat.resetNumDo();
          stat.resetScore();
          Navigator.push(
            context,
            // MaterialPageRoute(
            //     builder: (context) => const Addition(
            //           thisScore: 0,
            //           numDo: 0,
            //         )),
            MaterialPageRoute(builder: (context) => const Addition()),
          );
        },
      ),
      _MenuButton(
        icon: CustomIcons.minus_outline,
        title: 'Substract',
        color: Colors.green,
        onPressed: () {
          isStop.getStartTimerNow();
          stat.resetNumDo();
          stat.resetScore();
          Navigator.push(
            context,
            // MaterialPageRoute(
            //     builder: (context) => const Substract(
            //           thisScore: 0,
            //           numDo: 0,
            //         )),
            MaterialPageRoute(builder: (context) => const Substract()),
          );
        },
      ),
      _MenuButton(
        icon: CustomIcons.cancel_outline,
        title: 'Multiply',
        color: Colors.pink,
        onPressed: () {
          isStop.getStartTimerNow();
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Multiply()),
          );
        },
      ),
      
      _MenuButton(
        icon: CustomIcons.divide_outline,
        title: 'Division',
        color: Colors.amber,
        onPressed: () {
          isStop.getStartTimerNow();
          stat.resetNumDo();
          stat.resetScore();
          Navigator.push(
            context,
            // MaterialPageRoute(
            //     builder: (context) => const Division(
            //           thisScore: 0,
            //           numDo: 0,
            //         )),
            MaterialPageRoute(builder: (context) => const Division()),
          );
        },
      ),
    ];

    final menuGrid = GridView.extent(
        //.count
        maxCrossAxisExtent: smallerDevice ? screenWidth : screenWidth / 4,
        // crossAxisCount: smallerDevice ? 1 : 2,
        childAspectRatio: smallerDevice ? 1.2 : 0.9,
        children: menuList);

    final menuGridM = GridView.extent(
        //.count
        maxCrossAxisExtent: heightDevice ? screenWidth / 2 : screenWidth,
        // crossAxisCount: smallerDevice ? 1 : 2,
        childAspectRatio: heightDevice ? 1.1 : 1.2,
        children: menuList);

    return Scaffold(
        backgroundColor: Colors.white,
        body: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Container(
              alignment: Alignment.center,
              constraints: const BoxConstraints(
                maxWidth: 1700,
              ),
              child: Column(
                children: <Widget>[
                  titleWidget(context, landscapeHeight),
                  Flexible(
                    child:
                        !smallerDevice && heightDevice ? menuGridM : menuGrid,
                  )
                ],
              ),
            )));
  }
}

Widget titleWidget(BuildContext context, bool landscapeHeight) {
  return Padding(
      padding: const EdgeInsets.only(top: 26.0, bottom: 10.0),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(
                CustomIconsArrow
                    .left_open_outline, //left_outline, //arrow_alt_circle_left,
                size: landscapeHeight ? 35 : 27,
                color: Colors.brown[700],
              ),
            ),
          ),
          Expanded(
              flex: 5,
              child: Text(
                "Choosing Math Game",
                // style: TextStyle(
                //   fontWeight: FontWeight.bold,
                //   color: Colors.brown,
                // ),
                style: GoogleFonts.ubuntuCondensed(
                    fontWeight: FontWeight.w700, color: Colors.brown[700]),
                textScaleFactor: landscapeHeight ? 2.7 : 2.2,
                textAlign: TextAlign.center,
              )),
          const Expanded(flex: 1, child: Text(""))
        ],
      ));
}

class _MenuButton extends StatelessWidget {
  final IconData icon;
  final String title;
  final Function() onPressed;
  final Color color;
  const _MenuButton({
    Key? key,
    required this.icon,
    required this.title,
    required this.onPressed,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final canFitTitle = constraints.maxWidth > 180;

        return canFitTitle
            ? Padding(
                padding: const EdgeInsets.all(10.0),
                child: ElevatedButton(
                  onPressed: onPressed,
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(color)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Icon(icon, size: 45),
                      ),
                      Text(
                        title,
                        // style: const TextStyle(fontWeight: FontWeight.bold),
                        style: GoogleFonts.ubuntuCondensed(
                          fontWeight: FontWeight.w600,
                        ),
                        textScaleFactor: 2.2,
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              )
            // : IconButton(
            //     onPressed: onPressed,
            //     style: ButtonStyle(
            //         backgroundColor:
            //             MaterialStateProperty.all(Colors.brown[700])),
            //     icon: Icon(icon, size: 45),
            //   );
            : Padding(
                padding: const EdgeInsets.all(10.0),
                child: ElevatedButton(
                  onPressed: onPressed,
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(color)),
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Icon(icon, size: 45),
                  ),
                ),
              );
        // ListTile(
        //     leading: icon,
        //     title: Text(title),
        //     onTap: onPressed,
        //   )
      },
    );
  }
}
