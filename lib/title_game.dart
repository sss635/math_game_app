import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
// import 'alert_dialog.dart';
import 'package:provider/provider.dart';
import 'model/manage_timer.dart';

class TitlePage extends StatelessWidget {
  final String title;
  // final Widget nowGame;
  final Icon iconBack;
  final bool landscapeHeight;
  // final BuildContext context;
  const TitlePage({
    Key? key,
    required this.title,
    // required this.nowGame,
    required this.iconBack,
    required this.landscapeHeight,
    // required this.context,
  }) : super(key: key);

  // Widget titleWidget(context) {
  @override
  Widget build(BuildContext context) {
    final isStop = Provider.of<ManageTimer>(context);
    return Padding(
      padding: landscapeHeight
          ? const EdgeInsets.only(top: 10.0, bottom: 5.0)
          : const EdgeInsets.only(top: 26.0, bottom: 10.0),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: IconButton(
                onPressed: () {
                  isStop.getStopTimerNow();
                  Navigator.pop(context);
                  // showAlertDialog(context, nowGame, const MenuGame(),
                  //     "Back to Menu Game !!", "Are you sure !?");
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(builder: (context) => const MenuGame()),
                  // );
                },
                icon: iconBack),
          ),
          Expanded(
            flex: 5,
            child: Text(
              title,
              // style: const TextStyle(
              //   fontWeight: FontWeight.bold,
              //   color: Colors.brown,
              // ),
              style: GoogleFonts.ubuntuCondensed(
                fontWeight: FontWeight.w600,
                color: Colors.brown[700],
              ),
              textScaleFactor: landscapeHeight ? 1.8 : 2.3,
              textAlign: TextAlign.center,
            ),
          ),
          const Expanded(flex: 1, child: Text(""))
        ],
      ),
    );
  }
}
