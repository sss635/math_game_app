import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'model/manage_timer.dart';
import 'model/score_status.dart';

// import 'menu_game.dart';

Future<void> showAlertDialog(BuildContext context, sayNo, Widget sayYes,
    String title, String message) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      final isStopm = Provider.of<ManageTimer>(context);
      final stat = Provider.of<ScoreStatus>(context);
      return AlertDialog(
        backgroundColor: Colors.brown[50],
        // <-- SEE HERE
        title: Text(title,
            // style: const TextStyle(
            //     fontWeight: FontWeight.bold,
            //     fontSize: 20,
            //     color: Color.fromARGB(255, 66, 43, 35)),
            style: GoogleFonts.ubuntuCondensed(
                fontWeight: FontWeight.w600,
                color: Color.fromARGB(255, 66, 43, 35),
                fontSize: 24),
            textAlign: TextAlign.center),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(message,
                  // style: const TextStyle(
                  //   fontWeight: FontWeight.bold,
                  //   fontSize: 18,
                  //   color: Color.fromARGB(255, 66, 43, 35),
                  // ),
                  style: GoogleFonts.ubuntuCondensed(
                      fontWeight: FontWeight.w600,
                      color: Color.fromARGB(255, 66, 43, 35),
                      fontSize: 20),
                  textAlign: TextAlign.center),
            ],
          ),
        ),
        actions: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              TextButton(
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStatePropertyAll(Colors.brown[100])),
                child: Text(
                  'No',
                  // style: TextStyle(
                  //     color: Color.fromARGB(255, 112, 31, 25),
                  //     fontWeight: FontWeight.bold,
                  //     fontSize: 16),
                  style: GoogleFonts.ubuntuCondensed(
                      fontWeight: FontWeight.w600,
                      color: Color.fromARGB(255, 112, 31, 25),
                      fontSize: 16),
                  textAlign: TextAlign.center,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  if (sayNo != null) {
                    Navigator.pop(context);
                  }
                },
              ),
              TextButton(
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStatePropertyAll(Colors.brown[100])),
                child: Text(
                  'Yes',
                  // style: TextStyle(
                  //     color: Color.fromARGB(255, 66, 43, 35),
                  //     fontWeight: FontWeight.bold,
                  //     fontSize: 16),
                  style: GoogleFonts.ubuntuCondensed(
                      fontWeight: FontWeight.w600,
                      color: Color.fromARGB(255, 66, 43, 35),
                      fontSize: 16),
                  textAlign: TextAlign.center,
                ),
                onPressed: () {
                  isStopm.getStartTimerNow();
                  stat.resetNumDo();
                  stat.resetScore();
                  Navigator.of(context).pop();
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => sayYes),
                  );
                },
              ),
            ],
          )
        ],
      );
    },
  );
}
