import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'app_layout.dart';
import 'custom_icons.dart';
import 'package:provider/provider.dart';
import 'model/manage_timer.dart';
import 'model/score_status.dart';

import 'menu_game.dart';
import 'title_game.dart';
import 'challenge_question.dart';
import 'choice_button.dart';
import 'timer_game.dart';
import 'dart:math';
import 'alert_dialog.dart';
import 'dart:async';

double sizeTextChl = 2.5;
Color? colorChoiceBtn = Colors.brown[700];
int num1 = 0;
int num2 = 0;
int result = 0;
// bool _canShowButton = false;
// bool ori = true;
// int score = 0;
// int numQ = 0;
String title = "You're lose !!";
String message = "Play Again?";

// bool isStop = false;
class Substract extends StatelessWidget {
// class Substract extends StatefulWidget {
  const Substract({super.key});

  // final int thisScore;
  // final int numDo;
  // // const TimerGame({super.key});
  // const Substract({
  //   Key? key,
  //   required this.thisScore,
  //   required this.numDo,
  // }) : super(key: key);
  // @override
  // State<Substract> createState() =>
  //     _SubstractState(nowScore: this.thisScore, nowDo: this.numDo);
//   @override
//   State<Substract> createState() => _SubstractState();
// }

// class _SubstractState extends State<Substract> {
  // final int nowScore;
  // final int nowDo;

  // _SubstractState({
  //   Key? key,
  //   required this.nowScore,
  //   required this.nowDo,
  // });

  // @override
  // void initState() {
  //   super.initState();
  //   // ori = true;
  //   play(1); //play(0)
  //   score = nowScore;
  //   numQ = nowDo;
  //   // isStop = false;
  // }

  play() {
    // print("i $reset $ori");
    // if (reset == 0) {
    //   score = 0;
    //   numQ = 0;
    //   // print("y $reset");
    // }
    num1 = Random().nextInt(90) + 10;
    num2 = Random().nextInt(50) + 9;
    while (num1 - num2 < 0) {
      num1 = Random().nextInt(90) + 10;
      num2 = Random().nextInt(50) + 9;
    }
    result = num1 - num2;
  }

  // trOrFl(int num, int randomCr, context) {
  //   final stat = Provider.of<ScoreStatus>(context);
  //   if (num == randomCr) {
  //     stat.getScore(10);
  //     stat.getNumDo();
  //     // setState(() {
  //     // score += 10;
  //     // numQ++;
  //     // ori = false;
  //     // play(1);
  //     // });
  //     Navigator.pushReplacement(
  //       context,
  //       MaterialPageRoute(builder: (context) => const Substract()),
  //     );
  //   } else {
  //     stat.getScore(-12);
  //     stat.getNumDo();
  //     // setState(() {
  //     //   score -= 12;
  //     //   numQ++;
  //     //   // play(1);
  //     //   // ori = false;
  //     // });
  //     if (stat.score > 0) {
  //       Navigator.pushReplacement(
  //         context,
  //         MaterialPageRoute(builder: (context) => const Substract()),
  //       );
  //     }
  //   }
  // }

  genChoice(bool smallerDevice, bool heightDevice, BuildContext context) {
    final isStopm = Provider.of<ManageTimer>(context);
    final stat = Provider.of<ScoreStatus>(context);
    var ch = [0, 0, 0, 0];
    int randomCr = Random().nextInt(4);
    ch[randomCr] = result;
    var rd = 0;
    for (int i = 0; i < ch.length; i++) {
      if (i != randomCr) {
        rd = Random().nextInt(70) + 10;
        if (!ch.contains(rd)) {
          ch[i] = rd;
        }
      }
    }
    print(ch);
    print(randomCr);

    final choiceList = [
      ChoiceButton(
        num: '${ch[0]}',
        onPressed: () {
          // trOrFl(0, randomCr, context);
          if (0 == randomCr) {
            stat.getScore(10);
            stat.getNumDo();
            // Navigator.pushReplacement(
            //   context,
            //   MaterialPageRoute(builder: (context) => const Substract()),
            // );
          } else {
            stat.getScore(-12);
            stat.getNumDo();
          }
          if (stat.score > 0) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => const Substract()),
            );
          } else if (stat.score <= 0) {
            showAlertDialog(context, const MenuGame(), const Substract(), title,
                "\nCongratulation !! \nYou Do : ${stat.numDo} \nYour Score : ${stat.score} \nPlay Again?");
            isStopm.getStopTimerNow();
          }
        },
        colorBg: Colors.green[800],
      ),
      ChoiceButton(
        num: '${ch[1]}',
        onPressed: () {
          // trOrFl(1, randomCr, context);
          if (1 == randomCr) {
            stat.getScore(10);
            stat.getNumDo();
            // Navigator.pushReplacement(
            //   context,
            //   MaterialPageRoute(builder: (context) => const Substract()),
            // );
          } else {
            stat.getScore(-12);
            stat.getNumDo();
          }
          if (stat.score > 0) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => const Substract()),
            );
          } else if (stat.score <= 0) {
            showAlertDialog(context, const MenuGame(), const Substract(), title,
                "\nCongratulation !! \nYou Do : ${stat.numDo} \nYour Score : ${stat.score} \nPlay Again?");
            isStopm.getStopTimerNow();
          }
        },
        colorBg: Colors.green[600],
      ),
      ChoiceButton(
        num: '${ch[2]}',
        onPressed: () {
          // trOrFl(2, randomCr, context);
          if (2 == randomCr) {
            stat.getScore(10);
            stat.getNumDo();
            // Navigator.pushReplacement(
            //   context,
            //   MaterialPageRoute(builder: (context) => const Substract()),
            // );
          } else {
            stat.getScore(-12);
            stat.getNumDo();
          }
          if (stat.score > 0) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => const Substract()),
            );
          } else if (stat.score <= 0) {
            showAlertDialog(context, const MenuGame(), const Substract(), title,
                "\nCongratulation !! \nYou Do : ${stat.numDo} \nYour Score : ${stat.score} \nPlay Again?");
            isStopm.getStopTimerNow();
          }
        },
        colorBg: Colors.green[600],
      ),
      ChoiceButton(
        num: '${ch[3]}',
        onPressed: () {
          // trOrFl(3, randomCr, context);
          if (3 == randomCr) {
            stat.getScore(10);
            stat.getNumDo();
            // Navigator.pushReplacement(
            //   context,
            //   MaterialPageRoute(builder: (context) => const Substract()),
            // );
          } else {
            stat.getScore(-12);
            stat.getNumDo();
          }
          if (stat.score > 0) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => const Substract()),
            );
          } else if (stat.score <= 0) {
            showAlertDialog(context, const MenuGame(), const Substract(), title,
                "\nCongratulation !! \nYou Do : ${stat.numDo} \nYour Score : ${stat.score} \nPlay Again?");
            isStopm.getStopTimerNow();
          }
        },
        colorBg: Colors.green[800],
      ),
    ];

    final choiceGridS = GridView.count(
        crossAxisCount: smallerDevice ? 2 : 4,
        childAspectRatio: smallerDevice ? 1.2 : 1.3,
        children: choiceList);
    final choiceGridM = GridView.count(
        crossAxisCount: heightDevice ? 2 : 4,
        childAspectRatio: heightDevice ? 1.1 : 1.2,
        children: choiceList);

    final choiceGrid =
        !smallerDevice && heightDevice ? choiceGridM : choiceGridS;
    return choiceGrid;
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    final smallerDevice = screenWidth < AppLayout.widthFirstBreakpoint;
    final heightDevice = screenHeight > AppLayout.heightFirstBreakpoint;
    final landscapeHeight = screenHeight < AppLayout.widthFirstBreakpoint + 50;

    return Scaffold(
        backgroundColor: Colors.green[50],
        body: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Container(
              alignment: Alignment.center,
              constraints: const BoxConstraints(
                maxWidth: 1700,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(child: play()),
                  // titleWidget(context),
                  TitlePage(
                    title: "Substract Game",
                    iconBack: Icon(
                      CustomIconsArrow
                          .left_open_outline, //left_outline, //arrow_alt_circle_left,
                      size: landscapeHeight ? 27 : 35,
                      color: Colors.brown,
                    ),
                    landscapeHeight: landscapeHeight,
                  ),
                  // chalengeWidget(num1, num2),
                  const Divider(
                    color: Colors.brown,
                    thickness: 2,
                  ),
                  statusWidget(landscapeHeight, smallerDevice, context),
                  const Divider(
                    color: Colors.brown,
                    thickness: 2,
                  ),
                  ChallengeQuestion(
                    num1: num1,
                    iconOpr: const Icon(CustomIcons.minus_outline,
                        size: 25, color: Colors.brown),
                    num2: num2,
                    sizeTextChl: sizeTextChl,
                    iconAns: const Icon(
                        CustomIconsArrow
                            .help_circled_alt, //.question_1, //.question,
                        size: 40,
                        color: Colors.brown),
                    landscapeHeight: landscapeHeight,
                  ),
                  Flexible(
                    child: genChoice(smallerDevice, heightDevice, context),
                  ),
                ],
              ),
            )));
  }
}

Widget statusWidget(
    bool landscapeHeight, bool smallerDevice, BuildContext context) {
  return smallerDevice
      ? Column(
          children: [
            showScoreWidget(landscapeHeight, context),
            Container(
              height: landscapeHeight ? 30 : 50,
              child: const TimerGame(
                thisGame: Substract(),
                wSeconds: 7,
              ),
            ),
          ],
        )
      : Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 4,
              child: showScoreWidget(landscapeHeight, context),
            ),
            Expanded(
              flex: 3,
              child: Container(
                height: landscapeHeight ? 30 : 50,
                child: const TimerGame(
                  thisGame: Substract(),
                  wSeconds: 7,
                ),
              ),
            )
          ],
        );
}

Widget showScoreWidget(bool landscapeHeight, BuildContext context) {
  final stat = Provider.of<ScoreStatus>(context);
  return Container(
      // color: Color.fromARGB(255, 227, 187, 141),
      height: landscapeHeight ? 30 : 50,
      child: Center(
        child: Text.rich(
          TextSpan(
            // text: 'DO : $numQ', // default text style
            // style: TextStyle(
            //     fontSize: landscapeHeight ? 15 : 20,
            //     color: Colors.brown[700],
            //     fontWeight: FontWeight.bold),
            style: GoogleFonts.ubuntuCondensed(
                fontWeight: FontWeight.w600,
                color: Colors.brown[700],
                fontSize: landscapeHeight ? 20 : 25),
            children: <TextSpan>[
              const TextSpan(
                  text: 'DO : ', style: TextStyle(fontWeight: FontWeight.bold)),
              TextSpan(
                  text: ' ${stat.numDo}  | ',
                  style: TextStyle(
                    fontSize: landscapeHeight ? 25 : 30,
                  )),
              const TextSpan(
                  text: ' SCORE : ',
                  style: TextStyle(
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold)),
              TextSpan(
                  text: ' ${stat.score}',
                  style: TextStyle(
                      fontSize: landscapeHeight ? 25 : 30,
                      color: (stat.score > 5)
                          ? const Color.fromARGB(255, 26, 94, 29)
                          : const Color.fromARGB(255, 146, 42, 34))),
            ],
          ),
        ),
      ));
}


// Widget choiceGridWidget(int result) {
//   var ch = [0, 0, 0, 0];
//   int randomCr = Random().nextInt(4);
//   ch[randomCr] = result;
//   var rd = 0;
//   for (int i = 0; i < ch.length; i++) {
//     if (i != randomCr) {
//       rd = Random().nextInt(70) + 10;
//       if (!ch.contains(rd)) {
//         ch[i] = rd;
//       }
//     }
//   }

//   final choiceList = [
//     ChoiceButton(
//       num: '${ch[0]}',
//       onPressed: () {
//         if (0 == randomCr) {
//           _canShowButton = !_canShowButton;
//         }
//       },
//       colorBg: colorChoiceBtn,
//     ),
//     ChoiceButton(
//       num: '${ch[1]}',
//       onPressed: () {
//         if (1 == randomCr) {
//           _canShowButton = !_canShowButton;
//         }
//       },
//       colorBg: Colors.brown[800],
//     ),
//     ChoiceButton(
//       num: '${ch[2]}',
//       onPressed: () {
//         if (2 == randomCr) {
//           _canShowButton = !_canShowButton;
//         }
//       },
//       colorBg: Colors.brown[800],
//     ),
//     ChoiceButton(
//       num: '${ch[3]}',
//       onPressed: () {
//         if (3 == randomCr) {
//           _canShowButton = !_canShowButton;
//         }
//       },
//       colorBg: colorChoiceBtn,
//     ),
//   ];

//   final choiceGrid = GridView.count(
//       crossAxisCount: 2, childAspectRatio: 1.2, children: choiceList);
//   return choiceGrid;
// }

// Widget playNextWidget(BuildContext context) {
//   return Padding(
//       padding: const EdgeInsets.only(bottom: 16.0),
//       child: IconButton(
//         onPressed: () {
//           Navigator.push(
//             context,
//             MaterialPageRoute(builder: (context) => const Substract()),
//           );
//         },
//         icon: const Icon(Icons.play_circle_fill, size: 80, color: Colors.brown),
//         iconSize: 80,
//       ));
// }
