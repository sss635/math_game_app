import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
// import 'package:math_game_app/custom_icons.dart';

class ChallengeQuestion extends StatelessWidget {
  final int num1;
  final Icon iconOpr;
  final int num2;
  final double sizeTextChl;
  final Icon iconAns;
  final bool landscapeHeight;
  const ChallengeQuestion({
    Key? key,
    required this.num1,
    required this.iconOpr,
    required this.num2,
    required this.sizeTextChl,
    required this.iconAns,
    required this.landscapeHeight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double sizeChl = landscapeHeight ? 2.1 : sizeTextChl;
    return Padding(
      padding: landscapeHeight
          ? const EdgeInsets.all(10.0)
          : const EdgeInsets.all(20.0),
      child: Row(
        children: [
          Expanded(flex: landscapeHeight ? 2 : 1, child: const Text("")),
          Expanded(flex: 1, child: styleQuesWidget("$num1", sizeChl)),
          Expanded(flex: 1, child: iconOpr),
          Expanded(flex: 1, child: styleQuesWidget("$num2", sizeChl)),
          Expanded(flex: 1, child: styleQuesWidget("=", sizeChl + 0.7)),
          Expanded(flex: 1, child: iconAns),
          Expanded(flex: landscapeHeight ? 2 : 1, child: const Text(""))
        ],
      ),
    );
  }
}

Widget styleQuesWidget(String textQ, double sizeTextChl) {
  return Text(
    textQ,
    // style: const TextStyle(
    //   fontWeight: FontWeight.bold,
    //   color: Colors.brown,
    // ),
    style: GoogleFonts.ubuntuCondensed(
      fontWeight: FontWeight.w700,
      color: Colors.brown[700],
    ),
    textScaleFactor: sizeTextChl,
    textAlign: TextAlign.center,
  );
}
