abstract class AppLayout {
  static const double widthFirstBreakpoint = 450.0;
  static const double maximumContentWidthDetail = 700.0;
  static const double heightFirstBreakpoint = 850.0;
}
