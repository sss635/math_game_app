import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:math_game_app/menu_game.dart';

class HomePage extends StatelessWidget {
  final double scale;

  const HomePage({Key? key, required this.scale}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;
    String imgPortrait = "assets/bg-portrait.png";
    String imgLandscape = "assets/bg-landscape.png";
    return Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: isPortrait ? AssetImage(imgPortrait): AssetImage(imgLandscape),
                fit: BoxFit.cover),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Flexible(
                child: Text(
                  "SPEED MATHS",
                  style: GoogleFonts.ubuntuCondensed(
                      fontSize: isPortrait ? 60 * scale: 85 * scale,
                      fontWeight: FontWeight.w700,
                      color: Colors.pinkAccent
                  ),
                ),
                flex: 1,
              ),
              Flexible(
                child: SizedBox(
                  width: 150 * scale,
                  height: 150 * scale,
                  child: FittedBox(
                    child: FloatingActionButton(
                      backgroundColor: Colors.pinkAccent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const MenuGame()),
                        );
                      },
                      child: Icon(
                        Icons.play_arrow_rounded,
                        size: 40,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                flex: 2,
              ),
            ],
          ),
        )
    );
  }
}